import { useState } from "react";
import TableComponent from '../components/TableComponent'
import CheckboxComponent from '../components/CheckboxComponent'
import { tableData } from '../external/data'
import './App.css';

function App() {

  // const data = tableData

  // const [data, setUpdatedTablrData] = useState(tableData);
  const [isInstallments, setIsCheckedinstallments] = useState(false);
  const [isAvailable, setIsAvailable] = useState(false);



  function onChecked (status) {
    // console.log('event', status.name, status.checked)
    status.name == 'installmets' ? setIsCheckedinstallments(status.checked) : setIsAvailable(status.checked)
    // console.log('isInstallments: ', isInstallments)
    // console.log('isAvailable: ', isAvailable)
  }

  return (
    <div className="App">
      <CheckboxComponent name="installmets" label="Только в рассрочку" onChecked={onChecked} />
      <CheckboxComponent name="2" label="Есть в наличии" onChecked={onChecked} />
      <TableComponent data={tableData} options={[isInstallments, isAvailable]} />
    </div>
  );
}

export default App;
