import { useState } from "react";
import './CheckboxComponent.css'

function CheckboxComponent(props) {

  const [checked, setCheckedState] = useState(true);

  function handleChange () {
    setCheckedState(!checked)
    props.onChecked({ name: props.name, checked: checked })
  }
  
  return (
    <div>
        <input type="checkbox" id={props.name} onChange={handleChange}/>
        <p>{props.label}</p>
    </div>  
  );
}

export default CheckboxComponent;
