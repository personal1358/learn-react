import './TableComponent.css'

function TableComponent(props) {

  const opts = props.options
  
  const x = () => {
    if(opts[0] && opts[1]) return props.data.filter((item) => item.instalment && item.count != 0)
    if(opts[0]) return props.data.filter((item) => item.instalment)
    if(opts[1]) return props.data.filter((item) => item.count != 0)
    return props.data
  }
  
  const data =  x()

  console.log('sdfsdfsdfsdf:', props.options)

  return (
    <div className="table">
      <table className='tableName'>
        <tbody>
          <tr className='row'>
            <th>№</th>
            <th>Название</th>
            <th>Цена ↑</th>
            <th>Количество</th>
            <th>В рассрочку</th>
          </tr>
          {data.map(function(data, idx) {
            return(
              <tr className={data.count >=5 ? 'row' : 'lowAmount' } key={idx} >
                <td>{data.id}</td>
                <td>{data.name}</td>
                <td>{data.price}</td>
                <td>{data.count}</td>
                <td>{data.instalment ? '✅' : ''}</td>
              </tr>
            )
          })}
        </tbody>
      </table>
    </div>
  );
}

export default TableComponent;
